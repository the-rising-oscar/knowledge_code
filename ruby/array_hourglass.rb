#!/bin/ruby

require 'json'
require 'stringio'

# Complete the array2D function below.
def array2D(arr)
    sum = arr[0][0] + arr[0][1] + arr[0][2] + arr[1][1] + arr[2][0] + arr[2][1] + arr[2][2]
    temp_sum = 0
    for i in 0..6
        for j in 0..6
            if (j + 2 < 6 and i + 2 < 6)
                 temp_sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]
            end
            if (temp_sum >= sum)
                sum = temp_sum
            end
        end
    end
    return sum
end

fptr = File.open(ENV['OUTPUT_PATH'], 'w')

arr = Array.new(6)

6.times do |i|
    arr[i] = gets.rstrip.split(' ').map(&:to_i)
end

result = array2D arr

fptr.write result
fptr.write "\n"

fptr.close()
