#include <stdio.h>
int main()
{
    char test = 'c';
    int mask = 0x80; /* 10000000 */
    while (mask>0) {
        printf("%d", (test & mask) > 0 );
        mask >>= 1; /* move the bit down */
    }
}
