#include <stdio.h>
int main()
{
    int a = 12, b = 25;
    // a = 0000 1100
    // b = 0001 1001

    //     0000 1000
    printf("Output = %d\n", a&b);

    //     0001 1101
    printf("Output = %d\n", a|b);

    //     0001 0101
    printf("Output = %d\n", a^b);

    //     Bitwise complement of any number N is -(N+1)
    //     35 -> 0010 0011
    //     0010 0011
    //    +0000 0001
    //     0010 0100 -> 36 -> -36
    printf("complement = %d\n",~35);

    int num=212, i;
    for (i=0; i<=2; ++i)
        printf("Right shift by %d: %d\n", i, num>>i);

    printf("\n");
    for (i=0; i<=2; ++i)
        printf("Left shift by %d: %d\n", i, num<<i);

    return 0;
}
