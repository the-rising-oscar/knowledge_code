#include <stdio.h>

// this is ptr in c

int main () {

    // access address of variable
    int number = 10;
    printf( "Address of number variable: %p\n", &number );

    // array and ptr
    int array[5] = {1,2,3,4,5};
    printf( "The address of the first element in an array: %p\n", array );

    // assign address of variable to a ptr variable
    int *p_number = &number;
    printf( "Address of number variable: %p\n", p_number );

    // define a constant
    const int five = 5;

    return 0;
}


int check_iobuf(void)
{
    volatile int iobuf;
    int val;

    while (iobuf == 0) {
    }
    val = iobuf;
    iobuf = 0;
    return(val);
}
