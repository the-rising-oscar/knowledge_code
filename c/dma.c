#include<stdio.h>
#include<stdlib.h>
// malloc realloc calloc free	

int main()
{
	int *ptr;
	ptr = (int *)malloc(sizeof(int));

	if (ptr == NULL)
	{
		fprintf(stderr, "malloc failed\n");
		return 1;
	}

	ptr = realloc(ptr, 4 * sizeof(int));
	printf("%d\n", *ptr);
	printf("%d\n", *(ptr+1));
	printf("%d\n", *(ptr+2));
	printf("%d\n", *(ptr+3));
	free(ptr);
	ptr = calloc(1, sizeof (int));
	printf("\n%d\n", *ptr);
	free(ptr);
	return 0;
}
