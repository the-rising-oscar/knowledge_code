#include <stdio.h>
#include <stdlib.h>

void get_button_push( int button )
{
    // Declare a variable to store th bit mask used to select the desired bit
    int mask = 0x0;
    // Setup the mask based on the desired button (0 to 3)
    mask = 0x1;
    mask = mask << button;
    // resulting mask value:
    // 0x8 = 0000 1000(8) = button 3
    // 0x4 = 0000 0100(4) = button 2
    // 0x2 = 0000 0010(2) = button 1
    // 0x1 = 0000 0001(1) = button 0
    printf("mask value: %d",mask);
    printf("\n");

    // if ((0xf7 & mask) == 0) {
    //     printf("You pushed button %d\n", button); // button 3 is pushed, mask is 0x8
    // } else if ((0xfb & mask) == 0) {
    //     printf("You pushed button %d\n", button); // button 2 is pushed, mask is 0x4
    // } else if ((0xfd & mask) == 0) {
    //     printf("You pushed button %d\n", button); // button 1 is pushed, mask is 0x2
    // } else if ((0xfe & mask) == 0) {
    //     printf("You pushed button %d\n", button); // button 0 is pushed, mask is 0x1
    // } else {
    //     fprintf(stderr, "Invalid button?\n");
    //     exit(1);
    // }

    // Wait in a tight polling loop for the desired button to be pushed, e.g. button 0
    while((0xfe & mask) != 0);
    // Display on the console that the button was pushed
    printf("You pushed button %d\n", button);
    // Wait in a tight polling loop for the desired button to be released
    while((0xfe & mask) == 0);
}

int main()
{
    get_button_push( 0 ); // input: 0,1,2,3
    return 0;
}
