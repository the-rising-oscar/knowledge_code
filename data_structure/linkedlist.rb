require 'test/unit'
class Node
  attr_accessor :value, :next_node

	# Node constructor
  def initialize(value=nil)
    @value = value
    @next_node = nil
  end
end

class LinkedList
  attr_accessor :head, :tail

	# List constructor
  def initialize(head=nil,tail=nil)
    @head = head
    @tail = tail
  end

	# Returns the element at the given position
	def get_by_index(index, size)
		size = size - 1
		if index > size
			puts 'index out of bound'
		else
			ptr = @head
			pos = 0
			loop do
				if pos == index
					return ptr
				  break
				else
					pos += 1
					ptr = ptr.next_node
				end
			end
		end
	end

	# Print
  def to_ss
		str = ''
		ptr = @head
		until ptr == @tail
			str << "#{ptr} -> \n"
			ptr = ptr.next_node
		end
		str << "#{@tail}"
		puts str
  end

	# Appends an element to the list
  def append(node_with_value)
		node = node_with_value
		if @head.nil?
			@head = node
			@tail = node
		else
			ptr = @head
			until ptr.next_node.nil?
				ptr = ptr.next_node
			end
			ptr.next_node = node
			@tail = node
		end
	end

	# Remove last element from linked list
	def pop
		str = ''
		if @head.nil?
			puts 'empty'
		elsif @head.next_node.nil?
			puts "#{@head.value} " + 'popped'
			@head = nil
		else
			ptr = @head
		  until ptr.next_node.next_node.nil?
			  ptr = ptr.next_node
			end
			ptr.next_node = nil
			@tail = ptr
		end
	end

	# Returns the size of the list
	def size
    if @head.nil?
			puts 0
		elsif @head.next_node.nil?
      puts 1
		else
			ptr = @head
			count = 1
			until ptr.next_node.nil?
        count += 1
				ptr = ptr.next_node
			end
			return count
		end
	end

	# Outputs the index-value pairs
	# of all occurrences of a specified element
	def find_all(value)
	  ptr = @head
		pos = 0
		until ptr.nil?
			if ptr.value == value
				puts "pos:#{pos},node:#{ptr.value}"
			end
		  ptr = ptr.next_node
			pos += 1
		end
		puts 'not found'
	end

	# Returns head
	def get_head
		return @head
	end

	# Returns tail
	def get_tail
		return @tail
	end

	# Prepands an element to the list
	def prepand(value)
		new_head = Node.new(value)
		new_head.next_node = @head
		@head = new_head
	end

	# Removes the element
	# at the specified position and returns it
	def rm_by_index(index, size)
		size = size - 1
		if size < index
			puts 'index out of bound'
		elsif index == 0
			@head = @head.next_node
		else
			ptr = @head
			pos = 0
			node_to_be_rm = nil
			loop do
				if pos+1 == index
					node_to_be_rm = ptr.next_node
					node_to_be_concat = node_to_be_rm.next_node
					ptr.next_node = node_to_be_concat
					break
				else
					pos += 1
					ptr = ptr.next_node
					puts "current pos:#{pos}"
				end
			end
			output = node_to_be_rm
			node_to_be_rm = nil
			return output
		end
	end

	# inserts given value at given index
	# shifting subsequent values right
	def insert(index,value,size)
		node_to_insert = Node.new(value)
		size = size - 1
		if index > size
			puts 'index out of bound'
			return nil
		elsif index == size
			@tail.next_node = node_to_insert
			@tail = node_to_insert
		else
			pos = 0
			ptr = @head
			until pos + 1 == index
				ptr = ptr.next_node
				pos += 1
			end
			new_next_node = ptr.next_node
			ptr.next_node = node_to_insert
			node_to_insert.next_node = new_next_node
		end
	end

	# Removes the first element
	# that corresponds to the given object and returns it
	def rm_by_value(value)
		if @head.nil?
			puts 'empty'
		elsif @head.value == value
			@head = @head.next_node
		else
      ptr = @head
      until ptr.next_node.nil?
				if ptr.next_node.value == value
					popped = ptr.next_node
					new_next = ptr.next_node.next_node
					ptr.next_node = new_next
					return popped
				else
					ptr = ptr.next_node
				end
			end
		end
	end

	# Indicates if the list is empty
	def is_empty
		return @head == nil
	end

  # Clear the list
	def clear
		@head = nil
		@tail = nil
  end

  # Indicates if the list has founded a loop
	def is_loop
		fast_ptr = @head
		slow_ptr = @head
		until fast_ptr.next_node.nil?
			fast_ptr = fast_ptr.next_node
			slow_ptr = slow_ptr.next_node
			if fast_ptr.next_node != nil
				fast_ptr = fast_ptr.next_node
				if fast_ptr == slow_ptr
          puts 'loop found'
				  return true
			  end
			end
		end
		puts 'loop not found'
		return false
	end

  # Reverse the singly linked list
	def reverse
		new_next = nil
		cur = @head
		new_past = nil
		until cur.nil?
			new_past = cur.next_node
		  cur.next_node = new_next
			new_next = cur
			cur = new_past
		end
		tmp = @head
		@head = new_next
		@tail = tmp
		return @head.value
	end

  # Get the middle element of the list
	def get_middle_element
		fast_ptr = @head
		slow_ptr = @head
		until fast_ptr.next_node.nil?
			fast_ptr = fast_ptr.next_node
			slow_ptr = slow_ptr.next_node
			if fast_ptr.next_node != nil
				fast_ptr = fast_ptr.next_node
			end
		end
		return slow_ptr.value
	end

  # Get the last k element of the list
	def get_last_k_element(k,size)
		if k > size
			puts 'index out of bound'
		else
			pos = 0
			fast_p = @head
			slow_p = @head
			until pos == k - 1
			  fast_p = fast_p.next_node
				pos += 1
			end
			until fast_p.next_node.nil?
			  fast_p = fast_p.next_node
				slow_p = slow_p.next_node
			end
			return slow_p.value
		end
	end

  # Calculate the length of the loop
	def get_loop_length(first_collide_point)
		collide_node = first_collide_point
		if collide_node == nil
			return 0
		end
		length = 0
		slow_ptr = collide_node
		until slow_ptr.next_node.nil?
			slow_ptr = slow_ptr.next_node
			length += 1
			if slow_ptr == collide_node
				return length
			end
		end
	end

  # Calculate the first collide point
	def find_first_collide_point
		if is_loop == false
			return nil
		else
			collide_node = nil
			fast_ptr = @head
			slow_ptr = @head
			until fast_ptr.next_node.nil?
				fast_ptr = fast_ptr.next_node
				slow_ptr = slow_ptr.next_node
				if fast_ptr.next_node != nil
					fast_ptr = fast_ptr.next_node
					if fast_ptr == slow_ptr
						collide_node = slow_ptr
						break
					end
				end
			end
			return collide_node
		end
	end

	def find_loop_entrance(first_collide_point)
		collide_node = first_collide_point
		if collide_node == nil
			return collide_node
		end
		ptr1 = collide_node
		ptr2 = @head
		until ptr1 == ptr2
			ptr1 = ptr1.next_node
			ptr2 = ptr2.next_node
		end
		return ptr1
	end

	# def intersec(ll1,ll2)
	# 	head1 = ll1.get_head
	# 	head2 = ll2.get_head
	# 	ary1 = Array.new
	# 	ary2 = Array.new
	# 	until head1.nil?
	# 	  ary1 << head1
	# 		head1 = head1.next_node
	# 	end
	# 	until head2.nil?
	# 	  ary2 << head2
	# 		head2 = head2.next_node
	# 	end
	# 	intersec = ary1 & ary2
	# 	if intersec.empty?
	# 		puts 'no intersec'
	# 		return nil
	# 	else
	# 		puts ary1
	# 		puts "\n"
	# 		puts ary2
	# 		puts "\n"
    #   return intersec
	#   end
	# end

	def intersec(ll1,ll2)
		head1 = ll1.get_head
		head2 = ll2.get_head
	  hash1 = Hash.new
		hash2 = Hash.new
		until head1.nil?
		  hash1[head1.to_s] = head1
			head1 = head1.next_node
		end
		until head2.nil?
			hash2[hash2.to_s] = head2
			head2 = head2.next_node
		end
		intersec = Array.new
		hash1.each do |key, value|
      if hash2.has_value?(value)
				intersec << value
			end
		end
		if intersec.empty?
			puts 'no intersec'
			return nil
		else
  		puts ll1.to_ss
      puts ll1.get_tail.next_node
			puts "\n"
			puts ll2.to_ss
			puts "\n"
      return intersec
	  end
	end

end

ll1 = LinkedList.new
ll1.append(Node.new(1))
ll1.append(Node.new(2))
ll1.append(Node.new(3))








class TestLinkedList < Test::Unit::TestCase
#  def test_append
#    assert_not_nil(4, Node.new(1))
#    assert_not_nil(6, Node.new(2))
#  end
end
