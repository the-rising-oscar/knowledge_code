# O(1) space
# in-place ✅
# ascending / descending ✅
def insertion_sort(arr)
    if arr.length < 2
        return
    end
    i = 1
    while i < arr.length
        j = i
        while j != 0 and arr[j-1] > arr[j]
            arr[j],arr[j-1] = arr[j-1],arr[j]
            j = j - 1
        end
        i = i + 1
    end
end


arr = Array.new(10) { rand(1...100) }
p arr

insertion_sort(arr)
p arr
